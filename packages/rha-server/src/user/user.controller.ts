import { Controller, Get, Post, Body, Param, UseGuards } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { hasRoles } from 'src/auth/decorator/roles.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { UserDto } from './entities/user.dto';
import { User, UserRole } from './entities/user.interface';
import { UserAggregateService } from './user-aggregate/user-aggregate.service';

@Controller('user')
export class UserController {
  constructor(private readonly userAggregateService: UserAggregateService) {}

  @Post('create')
  create(@Body() user: UserDto): Observable<User | Object> {
    return this.userAggregateService.create(user);
  }

  @Post('login')
  login(@Body() user: User): Observable<Object> {
    return this.userAggregateService.login(user).pipe(
      map((jwt: string) => {
        return { access_token: jwt };
      }),
    );
  }

  @hasRoles(UserRole.ADMIN)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get('list')
  list(): Observable<User[]> {
    return this.userAggregateService.list();
  }

  @Get('fetch/:uuid')
  find(@Param('uuid') uuid: string): Observable<User> {
    return this.userAggregateService.find(uuid);
  }

  @Post('update')
  update(@Body() user: UserDto): Observable<any> {
    return this.userAggregateService.update(user);
  }

  @hasRoles(UserRole.ADMIN)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Post('update/role')
  updateRole(@Body() user: UserDto): Observable<User> {
    return this.userAggregateService.update(user);
  }

  @Post('remove/:uuid')
  remove(@Param('uuid') uuid: string): Observable<any> {
    return this.userAggregateService.remove(uuid);
  }
}
