import { IsBoolean, IsOptional, IsString, IsUUID } from 'class-validator';
import { v4 as uuidv4 } from 'uuid';

export class OccasionDto {
  @IsUUID()
  @IsOptional()
  uuid?: uuidv4;

  @IsString()
  @IsOptional()
  title?: string;

  @IsString()
  @IsOptional()
  description: string;

  @IsBoolean()
  @IsOptional()
  status: boolean;

  @IsString()
  @IsOptional()
  date: Date;

  @IsString()
  @IsOptional()
  startTime: string;

  @IsString()
  @IsOptional()
  endTime: string;
}
