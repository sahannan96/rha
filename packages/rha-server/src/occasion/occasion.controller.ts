import { Controller, Get, Post, Body, Param } from '@nestjs/common';
import { Observable } from 'rxjs';
import { OccasionDto } from './entities/occasion.dto';
import { Occasion } from './entities/occasion.interface';
import { OccasionAggregateService } from './occasion-aggregate/occasion-aggregate.service';

@Controller('occasion')
export class OccasionController {
  constructor(
    private readonly occasionAggregateService: OccasionAggregateService,
  ) {}

  @Post('create')
  create(@Body() occasion: OccasionDto): Observable<Occasion> {
    return this.occasionAggregateService.create(occasion);
  }

  @Get('list')
  list(): Observable<Occasion[]> {
    return this.occasionAggregateService.list();
  }

  @Get('fetch/:uuid')
  find(@Param('uuid') uuid: string): Observable<Occasion> {
    return this.occasionAggregateService.find(uuid);
  }

  @Post('update')
  update(@Body() occasion: OccasionDto): Observable<any> {
    return this.occasionAggregateService.update(occasion);
  }

  @Post('remove/:uuid')
  remove(@Param('uuid') uuid: string): Observable<any> {
    return this.occasionAggregateService.remove(uuid);
  }
}
