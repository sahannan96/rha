import { Component, OnInit } from '@angular/core';
import { LIST_USERS } from 'src/app/constants/url-strings';
import { User } from 'src/app/interfaces/user';
import { ListingService } from 'src/app/services/listing.service';
import { environment } from 'src/environments/environment';
import { Location } from '@angular/common';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.page.html',
  styleUrls: ['./leaderboard.page.scss'],
})
export class LeaderboardPage implements OnInit {
  users: User[] = [];
  currentUser: string;

  constructor(
    private readonly listingService: ListingService,
    private readonly location: Location,
    private readonly authService: AuthService
  ) {}

  ngOnInit() {
    this.getUsers();
    this.authService.getUserId().then((data) => {
      this.currentUser = data;
      console.log(this.currentUser);
    });
  }

  navigateBack() {
    this.location.back();
  }

  getUsers() {
    const url = environment.serverUrl + LIST_USERS;
    this.authService.getToken().then((token) => {
      this.listingService.getRequest(url, undefined, token).subscribe({
        next: (users) => {
          this.users = users;
          this.users = this.users.sort(
            (a, b) => 0 - (a.drivesAttended > b.drivesAttended ? 1 : -1)
          );
        },
        error: (err) => {
          console.log(err);
        },
      });
    });
  }
}
