import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FETCH_USER } from 'src/app/constants/url-strings';
import { User } from 'src/app/interfaces/user';
import { ListingService } from 'src/app/services/listing.service';
import { environment } from 'src/environments/environment';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  uuid = '';
  user: User;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly authService: AuthService,
    private readonly listingService: ListingService,
    private readonly location: Location
  ) {}

  navigateBack() {
    this.location.back();
  }

  ngOnInit() {
    this.uuid = this.route.snapshot.params.uuid;
    this.getUserData();
  }

  getUserData() {
    const url = environment.serverUrl + FETCH_USER + this.uuid;
    this.authService.getToken().then((token) => {
      this.listingService.getRequest(url, undefined, token).subscribe({
        next: (success: User) => {
          console.log(success);
          this.user = success;
        },
        error: (error) => {
          console.log(error);
        },
      });
    });
  }
}
