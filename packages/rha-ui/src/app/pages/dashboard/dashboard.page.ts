import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ACCESS_TOKEN } from 'src/app/constants/strings';
import { AuthService } from 'src/app/services/auth.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  loggedIn: boolean = false;
  token = '';
  user: string = '';
  constructor(
    private readonly storage: StorageService,
    private readonly authService: AuthService,
    private router: Router
  ) {}

  async ngOnInit() {
    this.user = await this.authService.getUserId();
    if (this.user) {
      this.loggedIn = true;
    } else {
      this.loggedIn = false;
      this.router.navigate(['/splash-screen']);
    }
  }

  logout() {
    this.storage
      .removeItem(ACCESS_TOKEN)
      .then(() => this.router.navigate(['/splash-screen']));
  }
}
