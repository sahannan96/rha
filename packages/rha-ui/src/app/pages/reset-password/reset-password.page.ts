import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PasswordMatcher } from './password.matcher';
import { environment } from 'src/environments/environment';
import { RESET_PASSWORD } from 'src/app/constants/url-strings';
import { ListingService } from 'src/app/services/listing.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.page.html',
  styleUrls: ['./reset-password.page.scss'],
})
export class ResetPasswordPage implements OnInit {
  hideCurrentPassword = true;
  hideNewPassword = true;
  hideConfirmPassword = true;

  form = new FormGroup(
    {
      currentPassword: new FormControl('', [Validators.required]),
      newPassword: new FormControl('', [Validators.required]),
      confirmPassword: new FormControl('', [Validators.required]),
    },
    this.checkPasswords
  );

  matcher = new PasswordMatcher();

  constructor(
    private readonly authService: AuthService,
    private readonly listingService: ListingService,
    private readonly location: Location
  ) {}

  ngOnInit() {}

  navigateBack() {
    this.location.back();
  }

  checkPasswords(group: FormGroup) {
    return group.controls.newPassword.value ===
      group.controls.confirmPassword.value
      ? null
      : { notSame: true };
  }

  ResetPassword() {
    // const currentPassword = this.form.controls.currentPassword.value;
    // const newPassword = this.form.controls.newPassword.value;
    // const confirmPassword = this.form.controls.confirmPassword.value;
    const url = environment.serverUrl + RESET_PASSWORD;
    const passwords = {
      currentPassword: this.form.controls.currentPassword.value,
      newPassword: this.form.controls.newPassword.value,
      confirmPassword: this.form.controls.confirmPassword.value,
    };
    console.log(passwords);
  }
}
