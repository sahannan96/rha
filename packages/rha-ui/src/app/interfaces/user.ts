export class User {
  uuid?: string;
  name?: string;
  profileImage: string;
  phone?: string;
  email?: string;
  password?: string;
  role?: string;
  dateOfJoining?: Date;
  drivesAttended?: number;
}
