import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-page-one',
  templateUrl: './page-one.page.html',
  styleUrls: ['./page-one.page.scss'],
})
export class PageOnePage implements OnInit {
  uuid = '';
  constructor(
    private readonly authService: AuthService,
    private readonly router: Router
  ) {}

  async ngOnInit() {
    this.router.events.subscribe({
      next: async (event: RouterEvent) => {
        if (event instanceof NavigationEnd) {
          this.uuid = await this.authService.getUserId();
        }
      },
    });
  }

  logout() {
    this.authService.removeToken().then(() => this.router.navigate(['/']));
  }
}
