import { ApplicationRef, Component, HostBinding, OnInit } from '@angular/core';
import { StorageService } from './services/storage.service';
import { DARK_MODE, DARK_MODE_CLASS_NAME, ON } from './constants/strings';
import { OverlayContainer } from '@angular/cdk/overlay';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'rha-ui';
  @HostBinding() class = '';

  constructor(
    private readonly storage: StorageService,
    private readonly overlay: OverlayContainer,
    private readonly ref: ApplicationRef
  ) {}

  // slideOpts = {
  //   initialSlide: 1,
  //   speed: 400,
  // };

  ngOnInit() {
    this.storage.getItem(DARK_MODE).then((darkMode) => {
      if (darkMode === ON) {
        this.class = DARK_MODE_CLASS_NAME;

        this.overlay.getContainerElement().classList.add(this.class);
        document.body.setAttribute('color-theme', 'dark');
        this.ref.tick();
      } else {
        this.class = '';
        this.overlay
          .getContainerElement()
          .classList.remove(DARK_MODE_CLASS_NAME);
        document.body.setAttribute('color-theme', 'light');
        this.ref.tick();
      }
    });
  }
}
