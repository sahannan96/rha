import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';
import jwt_decode from 'jwt-decode';
import { ACCESS_TOKEN } from '../constants/strings';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private readonly storage: StorageService) {}

  async getUserId() {
    const token = await this.storage.getItem(ACCESS_TOKEN);
    const user: any = jwt_decode(token);
    return user.user.uuid;
  }

  async getToken() {
    return await this.storage.getItem(ACCESS_TOKEN);
  }

  async setToken(token: string) {
    this.storage.setItem(ACCESS_TOKEN, token);
  }

  async removeToken() {
    return this.storage.removeItem(ACCESS_TOKEN);
  }
}
