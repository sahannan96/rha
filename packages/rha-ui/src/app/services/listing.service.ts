import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AUTHORIZATION, BEARER_HEADER_PREFIX } from '../constants/strings';

@Injectable({
  providedIn: 'root',
})
export class ListingService {
  constructor(private http: HttpClient) {}

  postRequest(url: string, payload?) {
    return this.http.post<any>(url, payload);
  }

  getRequest(url: string, params?, token?) {
    const headers = { [AUTHORIZATION]: BEARER_HEADER_PREFIX + token };

    return this.http.get<any>(url, { params, headers });
  }
}
